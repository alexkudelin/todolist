﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Input;
using TodoList.Entities;

namespace TodoList
{
    namespace ApplicationView
    {
        public class Card : Border
        {
            private String SHOW_MASK = "Показать задачи ({0})",
                           HIDE_MASK = "Скрыть задачи ({0})";

            private Task task = null;
            private MainWindow master = null;

            private Grid root = new Grid()
            {
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Center
            };

            private Label dateTimeLabel = new Label()
            {
                FontSize = 11,
                FontWeight = FontWeight.FromOpenTypeWeight(750),
                VerticalAlignment = VerticalAlignment.Top,
                VerticalContentAlignment = VerticalAlignment.Top,
                Foreground = new SolidColorBrush(Colors.DarkSlateBlue),
                Visibility = Visibility.Visible
            };

            private Label titleLabel = new Label()
            {
                Height = 25,
                FontSize = 16,
                FontWeight = FontWeight.FromOpenTypeWeight(999),
                VerticalAlignment = VerticalAlignment.Top,
                VerticalContentAlignment = VerticalAlignment.Top,
                Foreground = new SolidColorBrush(Colors.Black),
                Padding = new Thickness(0, -5, 0, -5),
                Margin = new Thickness(5, 0, 0, 0),
                Visibility = Visibility.Visible
            };

            private CheckBox taskCheckBox = new CheckBox()
            {
                VerticalContentAlignment = VerticalAlignment.Center,
                Margin = new Thickness(5, 0, 0, 0),
                Visibility = Visibility.Visible
            };

            private Label bodyLabel = new Label()
            {
                Height = 25,
                FontSize = 13,
                FontWeight = FontWeight.FromOpenTypeWeight(600),
                VerticalAlignment = VerticalAlignment.Top,
                VerticalContentAlignment = VerticalAlignment.Center,
                Foreground = new SolidColorBrush(Colors.Black),
                Padding = new Thickness(0, -5, 0, -5),
                Visibility = Visibility.Visible
            };

            private Button showMoreTasks = new Button()
            {
                Height = 25,
                FontSize = 13,
                Width = 150,
                FontWeight = FontWeight.FromOpenTypeWeight(500),
                VerticalAlignment = VerticalAlignment.Top,
                VerticalContentAlignment = VerticalAlignment.Center,
                Background = new SolidColorBrush(Colors.WhiteSmoke),
                BorderBrush = new SolidColorBrush(Colors.WhiteSmoke),
                BorderThickness = new Thickness(0)
            };

            private Grid childTaskGrid = new Grid()
            {
                Height = 0,
                VerticalAlignment = VerticalAlignment.Top,
                Visibility = Visibility.Hidden,
                Margin = new Thickness(10, 0, 0, 0)
            };

            private Ellipse priorityMarker = null;

            public Card(Task task, MainWindow master, double height = 105, double width = 450) : base()
            {
                this.task = task;
                this.master = master;

                this.CardHeight = height;
                this.CardWidth = width;

                this.priorityMarker = new PriorityMarker(this.task.Priority, 15).Marker;

                SetLayout();
                SetContent();

                this.Child = this.root;
            }

            private void SetLayout()
            {
                this.CornerRadius = new CornerRadius(5);
                this.Background = new SolidColorBrush(Colors.WhiteSmoke);
                this.BorderBrush = new SolidColorBrush(Colors.LightSlateGray);
                this.BorderThickness = new Thickness(1.25);

                RowDefinition dateTimeRow = new RowDefinition() { Height = new GridLength(25) },
                              titleRow = new RowDefinition() { Height = new GridLength(25) },
                              bodyRow = new RowDefinition() { Height = new GridLength(25) },
                              showMoreTaskButtonRow = new RowDefinition() { Height = new GridLength(25) },
                              childTaskGridRow = new RowDefinition() { Height = new GridLength(0) };

                this.root.RowDefinitions.Add(dateTimeRow);
                this.root.RowDefinitions.Add(titleRow);
                this.root.RowDefinitions.Add(bodyRow);
                this.root.RowDefinitions.Add(showMoreTaskButtonRow);
                this.root.RowDefinitions.Add(childTaskGridRow);

                ColumnDefinition priorityMarkerColumn = new ColumnDefinition() { Width = dateTimeRow.Height },
                                 freeSpaceColumn = new ColumnDefinition() { Width = new GridLength(this.Width - priorityMarkerColumn.Width.Value) };

                this.root.ColumnDefinitions.Add(freeSpaceColumn);
                this.root.ColumnDefinitions.Add(priorityMarkerColumn);

                this.root.Children.Add(this.dateTimeLabel);
                Grid.SetRow(this.dateTimeLabel, 0);
                Grid.SetColumn(this.dateTimeLabel, 0);

                this.root.Children.Add(this.titleLabel);
                Grid.SetRow(this.titleLabel, 1);
                Grid.SetColumn(this.titleLabel, 0);
                Grid.SetColumnSpan(this.titleLabel, 1);

                this.root.Children.Add(this.taskCheckBox);
                Grid.SetRow(this.taskCheckBox, 2);
                Grid.SetColumn(this.taskCheckBox, 0);
                Grid.SetColumnSpan(this.taskCheckBox, 1);

                this.root.Children.Add(this.priorityMarker);
                Grid.SetRow(this.priorityMarker, 0);
                Grid.SetColumn(this.priorityMarker, 1);

                this.root.Children.Add(this.showMoreTasks);
                Grid.SetRow(this.showMoreTasks, 3);
                Grid.SetColumn(this.showMoreTasks, 0);
                Grid.SetColumnSpan(this.showMoreTasks, 1);

                this.childTaskGrid.Width = this.Width;
                this.childTaskGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = freeSpaceColumn.Width });

                this.root.Children.Add(this.childTaskGrid);
                Grid.SetRow(this.childTaskGrid, 4);
                Grid.SetColumn(this.childTaskGrid, 0);
            }
            
            private void UpdateChildTaskGrid()
            {
                this.childTaskGrid.Children.Clear();
                this.childTaskGrid.RowDefinitions.Clear();
                this.childTaskGrid.Height = 0;

                foreach (var child in this.task.Children)
                {
                    this.childTaskGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(25) });

                    var childTaskCheckBox = new ChildTaskCheckBox(child)
                    {
                        Content = child.Title,
                        Height = 25,
                        Width = this.Width + 50,
                        VerticalAlignment = VerticalAlignment.Center,
                        VerticalContentAlignment = VerticalAlignment.Center,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        HorizontalContentAlignment = HorizontalAlignment.Left,
                        Padding = new Thickness(5, 0, 0, 0),
                        FontSize = 13,
                        FontWeight = FontWeight.FromOpenTypeWeight(600),
                        IsChecked = child.State == 1,
                        Visibility = Visibility.Visible
                    };

                    childTaskCheckBox.Click += OnChangeChildTaskState;

                    this.childTaskGrid.Children.Add(childTaskCheckBox);

                    Grid.SetRow(childTaskCheckBox, this.childTaskGrid.Children.Count - 1);
                    Grid.SetColumn(childTaskCheckBox, 0);

                    this.childTaskGrid.Height += 25;
                }

                this.root.RowDefinitions[this.root.RowDefinitions.Count - 1].Height = new GridLength(25 * this.task.Children.Count);
            }

            private void SetContent()
            {
                this.dateTimeLabel.Content = String.Format("Выполнить до: {0}", this.task.DoUntil.ToShortDateString());
                this.titleLabel.Content = this.task.Title;
                this.bodyLabel.Content = this.task.Body;

                this.taskCheckBox.Content = this.bodyLabel;

                this.taskCheckBox.Click += (o, arg) => { OnChangeTaskState(null, new CheckAsDoneEventArgs()); };

                this.taskCheckBox.IsChecked = this.task.State == 1;

                ChangeTaskState(new CheckAsDoneEventArgs());

                ContextMenu priorityMarkers = new ContextMenu();
                for (short i = 0; i < 5; i++)
                {
                    PriorityMarker priorityMarkerChoice = new PriorityMarker(i, 15);
                    MenuItem priorityMarkerMenuItem = new MenuItem()
                    {
                        Icon = priorityMarkerChoice.Marker,
                        HorizontalContentAlignment = HorizontalAlignment.Left,
                        Width = 20
                    };

                    priorityMarkerMenuItem.Click += (o, arg) => { OnChangePriorityMarker(null, new PriorityMarkerChangeEventArgs(priorityMarkerChoice)); };

                    priorityMarkers.Items.Add(priorityMarkerMenuItem);
                }

                this.priorityMarker.ContextMenu = priorityMarkers;

                this.MouseRightButtonDown += OnShowContextMenu;

                this.showMoreTasks.Content = String.Format(this.SHOW_MASK, this.task.Children.Count);
                this.showMoreTasks.Click += OnClickShowChildTasksButton;

                if (this.task.Children.Count == 0)
                {
                    this.showMoreTasks.Visibility = Visibility.Hidden;
                    this.CardHeight -= 25;
                }

                this.UpdateChildTaskGrid();

                this.master.Update();
            }

            private void OnClickShowChildTasksButton(object sender, RoutedEventArgs e)
            {
                if (this.childTaskGrid.Visibility == Visibility.Hidden)
                {
                    this.ShowChildTasks();
                }
                else
                {
                    this.HideChildTasks();
                }

                this.master.Update();
            }

            private void HideChildTasks()
            {
                this.CardHeight -= (this.childTaskGrid.Children.Count * 25);
                this.showMoreTasks.Width += 30;
                this.showMoreTasks.Content = String.Format(this.SHOW_MASK, this.task.Children.Count);
                this.childTaskGrid.Visibility = Visibility.Hidden;
            }

            private void ShowChildTasks()
            {
                this.CardHeight += (this.childTaskGrid.Children.Count * 25);
                this.showMoreTasks.Width -= 30;
                this.showMoreTasks.Content = String.Format(this.HIDE_MASK, this.task.Children.Count);
                this.childTaskGrid.Visibility = Visibility.Visible;
            }

            private void OnShowContextMenu(object sender, MouseButtonEventArgs e)
            {
                ContextMenu options = new ContextMenu();

                MenuItem checkAsDoneOption = new MenuItem() { Header = "Отметить как выполненное" },
                         editOption = new MenuItem() { Header = "Редактировать" };

                checkAsDoneOption.Click += (o, arg) => { OnChangeTaskState(null, new CheckAsDoneEventArgs(true)); }; ;
                editOption.Click += OnEditOptionClick;

                options.Items.Add(checkAsDoneOption);
                options.Items.Add(editOption);

                this.ContextMenu = options;
            }

            private void OnEditOptionClick(object sender, RoutedEventArgs e)
            {
                CardEditWindow editWindow = new CardEditWindow(this.task);

                if ((bool)editWindow.ShowDialog())
                {
                    this.bodyLabel.Content = this.task.Body;
                    this.titleLabel.Content = this.task.Title;
                    this.priorityMarker.Fill = new PriorityMarker(this.task.Priority, 15).Marker.Fill;
                    this.dateTimeLabel.Content = String.Format("Выполнить до: {0}", this.task.DoUntil.ToShortDateString());
                    
                    if (this.childTaskGrid.Visibility == Visibility.Visible)
                    {
                        this.HideChildTasks();
                    }
                    else
                    {
                        this.showMoreTasks.Content = String.Format(this.SHOW_MASK, this.task.Children.Count);
                    }

                    this.UpdateChildTaskGrid();

                    if (this.showMoreTasks.Visibility == Visibility.Visible)
                    {
                        if (this.childTaskGrid.Children.Count == 0)
                        {
                            this.showMoreTasks.Visibility = Visibility.Hidden;
                            this.CardHeight -= 25;
                        }
                    }
                    else
                    {
                        if (this.childTaskGrid.Children.Count > 0)
                        {
                            this.showMoreTasks.Visibility = Visibility.Visible;
                            this.CardHeight += 25;
                        }
                    }

                    this.UpdateTask();

                    this.master.Update();
                }
            }

            private void OnChangePriorityMarker(object sender, PriorityMarkerChangeEventArgs e)
            {
                this.priorityMarker.Fill = e.Color;
                this.Task.Priority = e.Priority;

                this.UpdateTask();

                this.master.Update();
            }

            private void OnChangeChildTaskState(object sender, RoutedEventArgs e)
            {
                ChildTaskCheckBox checkBox = (ChildTaskCheckBox)sender;

                checkBox.Task.State = ((bool)checkBox.IsChecked) ? (short)States.DONE : (short)States.NOT_DONE;

                this.task.SortChildren();

                this.UpdateChildTaskGrid();

                this.UpdateTask();
            }

            private void ChangeTaskState(CheckAsDoneEventArgs e)
            {
                this.Task.State = ((bool)this.taskCheckBox.IsChecked) ? (short)States.DONE : (short)States.NOT_DONE;

                foreach (ChildTaskCheckBox child in this.childTaskGrid.Children)
                {
                    child.IsChecked = (child.Task.State == (short)States.DONE) ? true : false;
                }

                UpdateChildTaskGrid();

                if (e.FromContextMenu)
                {
                    this.taskCheckBox.IsChecked = !this.taskCheckBox.IsChecked;
                }

                if ((bool)this.taskCheckBox.IsChecked)
                {
                    if (this.childTaskGrid.Visibility == Visibility.Visible)
                    {
                        this.HideChildTasks();
                    }
                }
            }

            private void OnChangeTaskState(object sender, CheckAsDoneEventArgs e)
            {
                ChangeTaskState(e);
                this.UpdateTask();
                this.master.Update();
            }

            private void UpdateTask()
            {
                DatabaseService.DatabaseHelper.Update(this.task);
            }

            public double RootHeight { get => this.root.Height; set => this.root.Height = value; }
            public double RootWidth { get => this.root.Width; set => this.root.Width = value; }

            public double CardHeight
            {
                get => this.Height;
                set
                {
                    this.Height = value;
                    this.RootHeight = value;
                }
            }

            public double CardWidth
            {
                get => this.Width;
                set
                {
                    this.Width = value;
                    this.RootWidth = value;
                }
            }

            public Task Task { get => this.task; private set => this.task = value; }
        }
    }
}