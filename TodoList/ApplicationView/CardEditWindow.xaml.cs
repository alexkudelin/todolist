﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TodoList.Entities;

namespace TodoList
{
    namespace ApplicationView
    {
        public partial class CardEditWindow : Window
        {
            private Task task;
            private Task temp_task;

            private Grid childTaskGrid = new Grid();
            private Button addChildTaskButton = new Button()
            {
                Content = "+ подзадача",
                Height = 25,
                Width = 100,
                FontSize = 12,
                FontWeight = FontWeight.FromOpenTypeWeight(600),
                VerticalAlignment = VerticalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                HorizontalContentAlignment = HorizontalAlignment.Center,
            };

            private Ellipse priorityMarker = new Ellipse()
            {
                Height = 15,
                Width = 15
            };

            public CardEditWindow()
            {
                InitializeComponent();
            }

            public CardEditWindow(Task task)
            {
                InitializeComponent();

                this.task = task;
                this.temp_task = new Task(task);

                this.SetLayout();
                this.SetContent();
            }

            private void SetLayout()
            {
                this.priorityMarker.Fill = new PriorityMarker(this.task.Priority, 15).Marker.Fill;

                ContextMenu priorityMarkerBox = new ContextMenu()
                {
                    VerticalContentAlignment = VerticalAlignment.Center
                };

                for (short i = 0; i < 5; i++)
                {
                    PriorityMarker priorityMarkerChoice = new PriorityMarker(i, 15);
                    MenuItem priorityMarkerMenuItem = new MenuItem()
                    {
                        Icon = priorityMarkerChoice.Marker,
                        HorizontalContentAlignment = HorizontalAlignment.Left,
                        Width = 20
                    };

                    priorityMarkerMenuItem.Click += (o, arg) => { OnChangePriorityMarker(null, new PriorityMarkerChangeEventArgs(priorityMarkerChoice)); };

                    priorityMarkerBox.Items.Add(priorityMarkerMenuItem);
                }

                this.priorityMarker.ContextMenu = priorityMarkerBox;
                this.root.Children.Add(this.priorityMarker);
                Grid.SetRow(this.priorityMarker, 0);
                Grid.SetColumn(this.priorityMarker, 1);

                this.childTaskGrid.Width = this.Width - 104;
                this.childTaskGrid.Height = 0;
                this.childTaskGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(this.childTaskGrid.Width * 0.85) });
                this.childTaskGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(this.childTaskGrid.Width * 0.15) });
                this.childTaskGrid.HorizontalAlignment = HorizontalAlignment.Center;

                this.root.Children.Add(this.childTaskGrid);
                Grid.SetRow(this.childTaskGrid, 3);
                Grid.SetColumn(this.childTaskGrid, 0);
                Grid.SetColumnSpan(this.childTaskGrid, 2);

                this.titleTextBox.TextChanged += OnChangeTitle;
                this.bodyTextBox.TextChanged += OnChangeBody;

                this.addChildTaskButton.Click += OnAddChildTask;
            }

            private void OnChangeBody(object sender, TextChangedEventArgs e)
            {
                this.temp_task.Body = this.bodyTextBox.Text;
            }

            private void OnChangeTitle(object sender, TextChangedEventArgs e)
            {
                this.temp_task.Title = this.titleTextBox.Text;
            }

            private void SetContent()
            {
                this.dateTimePicker.SelectedDate = this.task.DoUntil;
                this.dateTimePicker.SelectedDateChanged += OnChangeDateTime;

                this.titleTextBox.Text = this.task.Title;
                this.bodyTextBox.Text = this.task.Body;

                this.UpdateChildTaskGrid();
            }

            private void UpdateChildTaskGrid()
            {
                this.childTaskGrid.Children.Clear();
                this.childTaskGrid.RowDefinitions.Clear();
                this.childTaskGrid.Height = 0;
                this.root.RowDefinitions[3].Height = new GridLength(1);
                this.Height = 240;

                foreach (var child in this.temp_task.Children)
                {
                    this.childTaskGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(27) });

                    TextBox childTaskTextBox = new TextBox()
                    {
                        Text = child.Title,
                        Height = 25,
                        Width = this.childTaskGrid.Width * 0.85 - 1,
                        FontSize = 12,
                        FontWeight = FontWeight.FromOpenTypeWeight(600),
                        VerticalAlignment = VerticalAlignment.Center,
                        VerticalContentAlignment = VerticalAlignment.Center,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        HorizontalContentAlignment = HorizontalAlignment.Left,
                        Margin = new Thickness(0, 1, 1, 1)
                    };

                    childTaskTextBox.TextChanged += OnChangeChildTaskTitle;

                    Button removeChildTaskButton = new Button()
                    {
                        Content = "Удалить",
                        Width = this.childTaskGrid.Width * 0.15 - 1,
                        Height = 25,
                        FontSize = 12,
                        FontWeight = FontWeight.FromOpenTypeWeight(600),
                        VerticalAlignment = VerticalAlignment.Center,
                        VerticalContentAlignment = VerticalAlignment.Center,
                        HorizontalAlignment = HorizontalAlignment.Right,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        Margin = new Thickness(1, 1, 0, 1)
                    };

                    removeChildTaskButton.Click += OnRemoveChildTask;

                    int rowIndex = this.childTaskGrid.Children.Count/2;

                    this.childTaskGrid.Children.Add(childTaskTextBox);
                    Grid.SetRow(childTaskTextBox, rowIndex);
                    Grid.SetColumn(childTaskTextBox, 0);

                    this.childTaskGrid.Children.Add(removeChildTaskButton);
                    Grid.SetRow(removeChildTaskButton, rowIndex);
                    Grid.SetColumn(removeChildTaskButton, 1);

                    this.childTaskGrid.Height += 27;
                    this.Height += 27;
                }

                this.childTaskGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(27) });
                this.childTaskGrid.Height += 27;
                this.Height += 27;

                this.childTaskGrid.Children.Add(this.addChildTaskButton);
                Grid.SetRow(this.addChildTaskButton, (this.childTaskGrid.Children.Count - 1)/2);
                Grid.SetColumn(this.addChildTaskButton, 0);
                Grid.SetColumnSpan(this.addChildTaskButton, 2);

                this.root.RowDefinitions[3].Height = new GridLength(27 * (this.childTaskGrid.Children.Count + 1)/2);
            }

            private void OnChangeChildTaskTitle(object sender, TextChangedEventArgs e)
            {
                int index = this.childTaskGrid.Children.IndexOf((TextBox)sender)/2;
                this.temp_task.Children[index].Title = ((TextBox)sender).Text;
            }

            private void OnRemoveChildTask(object sender, RoutedEventArgs e)
            {
                int index = (this.childTaskGrid.Children.IndexOf((Button)sender) - 1)/2;
                this.temp_task.Children.RemoveAt(index);

                this.UpdateChildTaskGrid();
            }

            private void OnChangeDateTime(object sender, SelectionChangedEventArgs e)
            {
                var dp = (DatePicker)sender;

                if (dp.SelectedDate <= DateTime.Now)
                {
                    dp.SelectedDate = DateTime.Now.AddDays(1);
                }

                this.temp_task.DoUntil = (DateTime)dp.SelectedDate;
            }

            private void OnChangePriorityMarker(object p, PriorityMarkerChangeEventArgs priorityMarker)
            {
                this.temp_task.Priority = priorityMarker.Priority;
                this.priorityMarker.Fill = priorityMarker.Color;
            }

            private void OnApplyChanges(object sender, RoutedEventArgs e)
            {
                this.DialogResult = true;

                this.task.Title = this.temp_task.Title;
                this.task.Subtitle = this.temp_task.Title;
                this.task.Body = this.temp_task.Body;
                this.task.DoUntil = this.temp_task.DoUntil;
                this.task.Priority = this.temp_task.Priority;
                this.task.Children = this.temp_task.Children;

                this.Close();
            }

            private void OnCancelChanges(object sender, RoutedEventArgs e)
            {
                this.DialogResult = false;
                this.Close();
            }
            private void OnAddChildTask(object sender, RoutedEventArgs e)
            {
                this.temp_task.Add(new Task());

                this.UpdateChildTaskGrid();
            }
        }
    }
}
