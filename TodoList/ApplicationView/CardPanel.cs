﻿using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;

namespace TodoList
{
    namespace ApplicationView
    {
        public class CardPanel : Canvas
        {
            public CardPanel(double height = 1, double width = 480) : base()
            {
                this.HorizontalAlignment = HorizontalAlignment.Left;
                this.VerticalAlignment = VerticalAlignment.Top;
                Height = 0;
            }

            public double PanelWidth
            {
                get => this.Width;
                set {
                    this.Width = value;
                }
            }

            public double PanelHeight
            {
                get => this.Height;
                set
                {
                    this.Height = value;
                }
            }

            public void Add(Card card)
            {
                double offset = 0;
                double space = 5;

                foreach (var child in this.Children)
                {
                    offset += (((Card)child).Height + space);
                }

                this.Children.Add(card);
                SetTop(card, offset);

                this.Height = offset + card.Height + space;
            }

            public void Add(List<Card> cards)
            {
                foreach (var card in cards)
                {
                    Add(card);
                }
            }
        }
    }
}
