﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using TodoList.Entities;

namespace TodoList
{
    public enum States
    {
        NOT_DONE = 0,
        DONE = 1,
        REMOVED = 2
    }

    enum SortingParameter
    {
        PriorityDesc = 0,
        NewlyFirst,
        ElderFirst,
        Approaching
    }

    namespace ApplicationView
    {
        class CheckBoxEventArgs : RoutedEventArgs
        {
            private Task task = null;

            public CheckBoxEventArgs(Task task) : base()
            {
                this.task = task;
            }

            public Task Task { get => this.task; private set => this.task = value; }
        }

        class PriorityMarkerChangeEventArgs : RoutedEventArgs
        {
            private short priority;
            private Brush color;

            public PriorityMarkerChangeEventArgs(PriorityMarker pm) : base()
            {
                this.priority = pm.Priority;
                this.color = pm.Marker.Fill;
            }

            public short Priority { get => this.priority; }
            public Brush Color { get => this.color; }
        }

        class CheckAsDoneEventArgs : RoutedEventArgs
        {
            private bool fromContextMenu;

            public CheckAsDoneEventArgs(bool fromContextMenu = false) : base()
            {
                this.fromContextMenu = fromContextMenu;
            }

            public bool FromContextMenu { get => this.fromContextMenu; }
        }

        class ChildTaskCheckBox : CheckBox
        {
            private Task task;

            public ChildTaskCheckBox(Task task) : base()
            {
                this.task = task;
            }

            public Task Task { get => this.task; set => this.task = value; }
        }
    }
}
