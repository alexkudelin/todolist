﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using TodoList.Entities;

namespace TodoList
{
    namespace ApplicationView
    {

        public partial class MainWindow : Window
        {
            private readonly BackgroundWorker worker = new BackgroundWorker();

            private List<Task> tasks = new List<Task>();
            private List<Card> cards = new List<Card>();

            private ProgressBar loadingBar = new ProgressBar();
            private CardPanel taskPanel = new CardPanel();

            public MainWindow()
            {
                InitializeComponent();
                SetLayout();

                this.loadingBar.Visibility = Visibility.Visible;
                this.loadingBar.Value = 0;

                worker.WorkerReportsProgress = true;
                worker.ProgressChanged += UpdateLoadingBarStatus;
                worker.DoWork += LoadTasks;
                worker.RunWorkerCompleted += OnLoadingEnds;

                worker.RunWorkerAsync();
            }

            private void SetLayout()
            {
                sortingBox.SelectedIndex = (int)SortingParameter.PriorityDesc;
                settingPanel.Children.Add(loadingBar);

                loadingBar.Height = 15;
                loadingBar.Width = 150;

                scrollViewer.Content = this.taskPanel;
                scrollViewer.CanContentScroll = true;

                taskPanel.Width = scrollViewer.Width;
                
                Grid.SetRow(loadingBar, 0);
                Grid.SetColumn(loadingBar, 0);

                this.MouseRightButtonDown += OnShowContextMenu;
            }

            private void OnShowContextMenu(object sender, MouseButtonEventArgs e)
            {
            }

            private void OnLoadingEnds(object sender, RunWorkerCompletedEventArgs e)
            {
                loadingBar.Visibility = Visibility.Hidden;                

                foreach (Task task in tasks)
                {
                    cards.Add(new Card(task, this));
                }

                this.tasks.Clear();

                Update();
            }

            private void UpdateLoadingBarStatus(object sender, ProgressChangedEventArgs e)
            {
                loadingBar.Value = e.ProgressPercentage;
            }

            private void LoadTasks(object sender, DoWorkEventArgs args)
            {
                DatabaseService.DatabaseHelper.GetParentTasks(tasks);

                foreach (Task task in tasks)
                {
                    DatabaseService.DatabaseHelper.GetChildrenTasks(task.Key, task.Children);
                    worker.ReportProgress((this.tasks.IndexOf(task) + 1) * 100 / this.tasks.Count);
                }
            }

            private void OnAddButtonClick(object sender, RoutedEventArgs e)
            {
                Task newTask = new Task();

                if ((bool)new CardEditWindow(newTask).ShowDialog())
                {
                    this.cards.Add(new Card(newTask, this));

                    DatabaseService.DatabaseHelper.Add(newTask);

                    this.Update();
                }
            }

            private void OnSortingBoxItemClick(object sender, RoutedEventArgs e)
            {
                SortingParameter sortingParameter = (SortingParameter)((ComboBox)sender).SelectedIndex;

                this.cards.Sort((c1, c2) =>
                {
                    int stateCompareResult = c1.Task.State.CompareTo(c2.Task.State);

                    if (stateCompareResult == 0)
                    {
                        switch (sortingParameter)
                        {
                            default: break;

                            case SortingParameter.PriorityDesc:
                                {
                                    stateCompareResult = - c1.Task.Priority.CompareTo(c2.Task.Priority);
                                    break;
                                }
                            case SortingParameter.NewlyFirst:
                                {
                                    stateCompareResult = -c1.Task.Created.CompareTo(c2.Task.Created);
                                    break;
                                }
                            case SortingParameter.ElderFirst:
                                {
                                    stateCompareResult = c1.Task.Created.CompareTo(c2.Task.Created);
                                    break;
                                }
                            case SortingParameter.Approaching:
                                {
                                    stateCompareResult = -c1.Task.DoUntil.CompareTo(c2.Task.DoUntil);
                                    break;
                                }
                        }
                    }

                    return stateCompareResult;
                });

                UpdateTaskPanel();
            }

            private void UpdateTaskPanel()
            {
                taskPanel.Children.Clear();
                taskPanel.Add(this.cards);
            }

            public void Update()
            {
                OnSortingBoxItemClick(this.sortingBox, null);
            }
        }
    }
}
