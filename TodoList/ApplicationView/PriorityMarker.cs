﻿using System.Windows.Media;
using System.Windows.Shapes;

namespace TodoList
{
    namespace ApplicationView
    {
        class PriorityMarker
        {
            private Ellipse marker = new Ellipse();
            private short priority;
            Color[] markerColors = { Colors.GreenYellow, Colors.Green, Colors.DodgerBlue, Colors.Orange, Colors.Red };

            public PriorityMarker(short priority, double diameter)
            {
                marker.Height = diameter;
                marker.Width = diameter;
                marker.Fill = new SolidColorBrush(markerColors[priority]);                
                marker.Stroke = new SolidColorBrush(Colors.LightGray);
                this.priority = priority;
            }

            public Ellipse Marker { get => this.marker; private set => this.marker = value; }
            public short Priority { get => this.priority; }
        }
    }
}
