﻿using System;
using System.Text;
using System.Data.Odbc;

namespace TodoList
{
    namespace DatabaseService
    {
        public sealed class DatabaseConnector : IDisposable
        {
            private string dsn = "MyPostgreSQLDSN",
                           host = "localhost",
                           port = "5432",
                           databaseName = "TodoList",
                           user = "postgres",
                           pwd = "postgres";

            private OdbcConnection connection = null;

            public DatabaseConnector()
            {
                String connectionString = "Dsn={0}; database={1}; server={2}; port={3}; uid={4}; pwd={5}";
                StringBuilder connectionStringBuilder = new StringBuilder();

                connectionString = connectionStringBuilder.AppendFormat(connectionString, this.dsn, this.databaseName, this.host, this.port, this.user, this.pwd).ToString();

                this.connection = new OdbcConnection(connectionString);
            }

            public OdbcConnection Connection { get => this.connection; private set => this.connection = value; }

            #region IDisposable Support
            private bool disposedValue = false; // To detect redundant calls

            void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        // TODO: dispose managed state (managed objects).
                    }

                    this.connection.Close();

                    this.connection = null;
                    disposedValue = true;
                }
            }

            // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
            ~DatabaseConnector()
            {
                // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
                Dispose(false);
            }

            // This code added to correctly implement the disposable pattern.
            public void Dispose()
            {
                // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
                Dispose(true);
                // TODO: uncomment the following line if the finalizer is overridden above.
                GC.SuppressFinalize(this);
            }
            #endregion
        }
    }
}
