﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Odbc;
using TodoList.DatabaseService.DatabaseInterfaces;
using TodoList.Entities;

namespace TodoList.DatabaseService
{
    public class DatabaseHelper : IAddable, IRemovable, IAccessible, IUpdatable
    {
        private readonly object locker = new object();

        #region Adding implementation
        public static bool Add(Task task)
        {
            return ((IAddable)new DatabaseHelper()).Add(task);
        }

        public static bool Add(Attachment attachment)
        {
            return ((IAddable)new DatabaseHelper()).Add(attachment);
        }
        #endregion

        #region IAddable implementation
        bool IAddable.Add(Task task)
        {
            try
            {

                lock (locker)
                {
                    using (var connection = new DatabaseConnector().Connection)
                    {
                        String sql = "INSERT INTO {0} ({1}) VALUES ({2}) RETURNING {3}";

                        String tableName = "Task";
                        String returningColumnName = Service.Escape("@" + tableName);

                        List<String> columnNamesList = new List<String>()
                        {
                            "ParentTask",
                            "Title",
                            "Subtitle",
                            "Body",
                            "DoUntil",
                            "State",
                            "Priority"
                         };
                        String columnNames = Service.JoinWithEscaping(columnNamesList);

                        List<String> columnValuesList = new List<String>()
                        {
                            task.ParentKey.ToString(),
                            Service.Escape(task.Title, "'"),
                            Service.Escape(task.Subtitle, "'"),
                            Service.Escape(task.Body, "'"),
                            Service.Escape(task.DoUntil.ToString(), "'") + "::timestamptz",
                            task.State.ToString(),
                            task.Priority.ToString()
                        };

                        String columnValues = Service.JoinWithoutEscaping(columnValuesList);
                        StringBuilder sqlBuilder = new StringBuilder();
                        sqlBuilder = sqlBuilder.AppendFormat(sql, Service.Escape(tableName), columnNames, columnValues, returningColumnName);

                        OdbcDataAdapter dataAdapter = new OdbcDataAdapter();
                        OdbcCommandBuilder queryBuilder = new OdbcCommandBuilder(dataAdapter);
                        dataAdapter.SelectCommand = new OdbcCommand(sqlBuilder.ToString(), connection);
                        DataSet queryResult = new DataSet();
                        dataAdapter.Fill(queryResult);

                        foreach (DataRow row in queryResult.Tables[0].Rows)
                        {
                            task.Key = Convert.ToInt64(row["@Task"]);

                            foreach (Task child in task.Children)
                            {
                                child.ParentKey = task.Key;
                                Add(child);
                            }
                        }
                    }
                }
                return true;
            }
            catch {
                return false;
            }
        }

        bool IAddable.Add(Attachment attachment)
        {
            try
            {
                lock (locker)
                {
                    using (var connection = new DatabaseConnector().Connection)
                    {
                        String sql = "INSERT INTO {0} ({1}) VALUES ({2}) RETURNING {3}";

                        String tableName = "Attachment";
                        String returningColumnName = Service.Escape("@" + tableName);

                        List<String> columnNamesList = new List<String>()
                    {
                        "Task",
                        "Data",
                        "Filename",
                        "Type"
                     };
                        String columnNames = Service.JoinWithEscaping(columnNamesList);

                        List<String> columnValuesList = new List<String>()
                    {
                        attachment.TaskKey.ToString(),
                        Service.Escape(attachment.Data, "'"),
                        Service.Escape(attachment.Filename, "'"),
                        Service.Escape(attachment.Type, "'")
                    };

                        String columnValues = Service.JoinWithoutEscaping(columnValuesList);
                        StringBuilder sqlBuilder = new StringBuilder();
                        sqlBuilder = sqlBuilder.AppendFormat(sql, Service.Escape(tableName), columnNames, columnValues, returningColumnName);

                        OdbcDataAdapter dataAdapter = new OdbcDataAdapter();
                        OdbcCommandBuilder queryBuilder = new OdbcCommandBuilder(dataAdapter);
                        dataAdapter.SelectCommand = new OdbcCommand(sqlBuilder.ToString(), connection);
                        DataSet queryResult = new DataSet();
                        dataAdapter.Fill(queryResult);

                        foreach (DataRow row in queryResult.Tables[0].Rows)
                        {
                            long key = Convert.ToInt64(row["@Attachment"]);
                            attachment.Key = key;
                            break;
                        }
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Getting implementation
        public static void Get(long key, out Task task)
        {
            ((IAccessible)new DatabaseHelper()).Get(key, out task);
        }

        public static void Get(long key, out Attachment attachment)
        {
            ((IAccessible)new DatabaseHelper()).Get(key, out attachment);
        }

        public static void Get(List<Attachment> attachments)
        {
            ((IAccessible)new DatabaseHelper()).Get(attachments);
        }

        public static void Get(long parentKey, List<Task> tasks)
        {
            ((IAccessible)new DatabaseHelper()).Get(parentKey, tasks);
        }

        public static void GetParentTasks(List<Task> parents)
        {
            ((IAccessible)new DatabaseHelper()).Get(0, parents);
        }

        public static void GetChildrenTasks(long parentKey, List<Task> children)
        {
            ((IAccessible)new DatabaseHelper()).Get(parentKey, children);
        }

        public static void GetAllTasks(List<Task> tasks)
        {
            ((IAccessible)new DatabaseHelper()).Get(tasks);
        }
        #endregion

        #region IAccessible implementation
        void IAccessible.Get(long key, out Task task)
        {
            task = null;

            lock (locker)
            {
                using (var connection = new DatabaseConnector().Connection)
                {
                    String sql = "SELECT * FROM {0} WHERE {1} = {2} LIMIT 1";
                    String tableName = "Task";
                    String columnName = Service.Escape("@" + tableName);
                    tableName = Service.Escape(tableName);
                    StringBuilder sqlBuilder = new StringBuilder();
                    sqlBuilder.AppendFormat(sql, tableName, columnName, key.ToString());

                    OdbcDataAdapter dataAdapter = new OdbcDataAdapter();
                    OdbcCommandBuilder queryBuilder = new OdbcCommandBuilder(dataAdapter);
                    dataAdapter.SelectCommand = new OdbcCommand(sqlBuilder.ToString(), connection);
                    DataSet queryResult = new DataSet();
                    dataAdapter.Fill(queryResult);

                    foreach (DataRow row in queryResult.Tables[0].Rows)
                    {
                        List<Task> children = new List<Task>();
                        Get(Convert.ToInt64(row["@Task"]), children);

                        task = new Task() {
                            Key = Convert.ToInt64(row["@Task"]),
                            ParentKey = Convert.ToInt64(row["ParentTask"]),
                            Title = Convert.ToString(row["Title"]),
                            Subtitle = Convert.ToString(row["Subtitle"]),
                            Body = Convert.ToString(row["Body"]),
                            Created = Convert.ToDateTime(row["Created"]),
                            DoUntil = Convert.ToDateTime(row["DoUntil"]),
                            Children = children,
                            State = Convert.ToInt16(row["State"]),
                            Priority = Convert.ToInt16(row["Priority"])
                        };
                    }                       
                }
            }
        }

        void IAccessible.Get(long key, out Attachment attachment)
        {
            attachment = null;

            lock (locker)
            {
                using (var connection = new DatabaseConnector().Connection)
                {
                    String sql = "SELECT * FROM {0} WHERE {1} = {2} LIMIT 1";
                    String tableName = "Attachment";
                    String columnName = Service.Escape("@" + tableName);
                    tableName = Service.Escape(tableName);
                    StringBuilder sqlBuilder = new StringBuilder();
                    sqlBuilder.AppendFormat(sql, tableName, columnName, key.ToString());

                    OdbcDataAdapter dataAdapter = new OdbcDataAdapter();
                    OdbcCommandBuilder queryBuilder = new OdbcCommandBuilder(dataAdapter);
                    dataAdapter.SelectCommand = new OdbcCommand(sqlBuilder.ToString(), connection);
                    DataSet queryResult = new DataSet();
                    dataAdapter.Fill(queryResult);

                    foreach (DataRow row in queryResult.Tables[0].Rows)
                    {
                        attachment = new Attachment(
                            Convert.ToInt64(row["@Attachment"]),
                            Convert.ToInt64(row["Task"]),
                            Convert.ToString(row["Data"]),
                            Convert.ToString(row["Filename"]),
                            Convert.ToString(row["Type"])
                        );
                    }
                }
            }
        }

        void IAccessible.Get(List<Attachment> attachments)
        {
            lock (locker)
            {
                using (var connection = new DatabaseConnector().Connection)
                {
                    String sql = "SELECT * FROM \"Attachment\"";

                    OdbcDataAdapter dataAdapter = new OdbcDataAdapter();
                    OdbcCommandBuilder queryBuilder = new OdbcCommandBuilder(dataAdapter);
                    dataAdapter.SelectCommand = new OdbcCommand(sql, connection);
                    DataSet queryResult = new DataSet();
                    dataAdapter.Fill(queryResult);

                    foreach (DataRow row in queryResult.Tables[0].Rows)
                    {
                        attachments.Add(
                            new Attachment(
                                Convert.ToInt64(row["@Attachment"]),
                                Convert.ToInt64(row["Task"]),
                                Convert.ToString(row["Data"]),
                                Convert.ToString(row["Filename"]),
                                Convert.ToString(row["Type"])
                            )
                        );
                    }
                }
            }
        }
        
        void IAccessible.Get(List<Task> tasks)
        {
            lock (locker)
            {
                using (var connection = new DatabaseConnector().Connection)
                {
                    GetParentTasks(tasks);

                    foreach (var parentTask in tasks)
                    {
                        List<Task> children = new List<Task>();
                        GetChildrenTasks(parentTask.Key, children);

                        foreach (var child in children)
                        {
                            parentTask.Add(child);
                        }
                    }
                }
            }
        }

        void IAccessible.Get(long parentKey, List<Task> tasks)
        {
            lock (locker)
            {
                using (var connection = new DatabaseConnector().Connection)
                {
                    String sql = String.Format("SELECT * FROM \"Task\" WHERE \"ParentTask\" = {0}", parentKey.ToString());

                    OdbcDataAdapter dataAdapter = new OdbcDataAdapter();
                    OdbcCommandBuilder queryBuilder = new OdbcCommandBuilder(dataAdapter);
                    dataAdapter.SelectCommand = new OdbcCommand(sql, connection);
                    DataSet queryResult = new DataSet();
                    dataAdapter.Fill(queryResult);

                    foreach (DataRow row in queryResult.Tables[0].Rows)
                    {
                        tasks.Add(
                            new Task() {
                                Key = Convert.ToInt64(row["@Task"]),
                                ParentKey = Convert.ToInt64(row["ParentTask"]),
                                Title = Convert.ToString(row["Title"]),
                                Subtitle = Convert.ToString(row["Subtitle"]),
                                Body = Convert.ToString(row["Body"]),
                                Created = Convert.ToDateTime(row["Created"]),
                                DoUntil = Convert.ToDateTime(row["DoUntil"]),
                                State = Convert.ToInt16(row["State"]),
                                Priority = Convert.ToInt16(row["Priority"])
                            }
                        );
                    }

                    tasks.Sort((t1, t2) => { return t1.State.CompareTo(t2.State); });
                }
            }
        }


        #endregion

        #region Removing implementation
        public static bool Remove(Task task)
        {
            return ((IRemovable)new DatabaseHelper()).Remove(task);
        }
        #endregion

        #region IRemovable implementation
        bool IRemovable.Remove(Task task)
        {
            bool result = false;

            lock (locker)
            {
                try
                {
                    using (var connection = new DatabaseConnector().Connection)
                    {
                        String sql = "DELETE FROM \"Task\" WHERE \"@Task\" = {2}";
                        StringBuilder sqlBuilder = new StringBuilder();

                        sqlBuilder.AppendFormat(sql, task.Key.ToString());

                        OdbcDataAdapter dataAdapter = new OdbcDataAdapter();
                        OdbcCommandBuilder queryBuilder = new OdbcCommandBuilder(dataAdapter);
                        dataAdapter.SelectCommand = new OdbcCommand(sqlBuilder.ToString(), connection);

                        result = true;
                    }
                }
                catch
                {
                    result = false;
                }
            }
            return result;
        }

        #endregion

        #region Updating implementation
        public static void Update(Task task)
        {
            ((IUpdatable)new DatabaseHelper()).Update(task);
        }
        #endregion

        #region IUpdatable implementation
        bool IUpdatable.Update(Task task)
        {
            bool result = false;

            lock (locker)
            {
                try
                {
                    // добавим сначала задачи, которых нет в принципе в БД
                    foreach (var child in task.Children.FindAll((t) => t.Key == 0))
                    {
                        Add(child);
                    }

                    // теперь удалим задачи, которые оказались удалены
                    List<int> keysToRemove = new List<int>();
                    List<Task> candidatesToRemove = new List<Task>();

                    GetChildrenTasks(task.Key, candidatesToRemove);

                    foreach (var candidate in candidatesToRemove)
                    {
                        if (task.Children.FindAll((t) => t.Key == candidate.Key).Count == 0)
                        {
                            Remove(candidate);
                        }
                    }

                    using (var connector = new DatabaseConnector())
                    {

                        OdbcCommand command = new OdbcCommand();
                        OdbcTransaction transaction = null;

                        connector.Connection.Open();

                        command.Connection = connector.Connection;
                        transaction = connector.Connection.BeginTransaction();
                        
                        command.Connection = connector.Connection;
                        command.Transaction = transaction;
                            
                        String sql = "UPDATE {0} SET {1} WHERE {2} = {3}";

                        String tableName = "Task";
                        String returningColumnName = Service.Escape("@" + tableName);

                        List<String> columnNamesList = new List<String>()
                        {
                            "ParentTask",
                            "Title",
                            "Subtitle",
                            "Body",
                            "DoUntil",
                            "State",
                            "Priority"
                        };

                        List<String> columnValuesList = new List<String>()
                        {
                            task.ParentKey.ToString(),
                            Service.Escape(task.Title, "'"),
                            Service.Escape(task.Subtitle, "'"),
                            Service.Escape(task.Body, "'"),
                            Service.Escape(task.DoUntil.ToString(), "'") + "::timestamptz",
                            task.State.ToString(),
                            task.Priority.ToString()
                        };

                        List<String> setValues = new List<String>();

                        for (int i = 0; i < columnNamesList.Count; i++)
                        {
                            setValues.Add(String.Format("{0} = {1}", Service.Escape(columnNamesList[i]), columnValuesList[i]));
                        }

                        String setStatements = String.Join(", ", setValues);

                        StringBuilder sqlBuilder = new StringBuilder();

                        command.CommandText = sqlBuilder.AppendFormat(sql, Service.Escape(tableName), setStatements, returningColumnName, task.Key.ToString()).ToString();
                        command.ExecuteNonQuery();

                        transaction.Commit();
                    }

                    foreach (var child in task.Children)
                    {
                        Update(child);
                    }

                    return true;
                }
                catch
                {
                    result = false;
                }
            }

            return result;
        }
        #endregion
    }
}
