﻿using System;
using System.Collections.Generic;
using TodoList.Entities;

namespace TodoList.DatabaseService.DatabaseInterfaces
{
    interface IAddable
    {
        bool Add(Task task);
        bool Add(Attachment attachment);
    }

    interface IAccessible
    {
        void Get(long key, out Task task);
        void Get(long key, out Attachment attachment);
        void Get(List<Attachment> attachments);
        void Get(List<Task> tasks);
        void Get(long parentKey, List<Task> tasks);
    }

    interface IRemovable
    {
        bool Remove(Task task);
    }

    interface IUpdatable
    {
        bool Update(Task task);
    }
}
