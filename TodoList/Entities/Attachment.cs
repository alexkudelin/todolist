﻿using System;
using System.Collections.Generic;

namespace TodoList.Entities
{
    public class Attachment
    {
        private long key;
        private long taskKey;
        private String data;
        private String filename;
        private String type;

        #region Constructors
        private void Init(long key = 0, long taskKey = 0, String data = "", String filename = "unknown.file", String type = "Неизвестный")
        {
            this.key = key;
            this.taskKey = taskKey;
            this.data = data;
            this.filename = filename;
            this.type = type;
        }

        public Attachment()
        {
            Init();
        }

        public Attachment(byte[] data, String filename, String type)
        {
            Init(data: Convert.ToBase64String(data), filename: filename, type: type);
        }

        public Attachment(List<byte> data, String filename, String type)
        {
            Init(data: Convert.ToBase64String(data.ToArray()), filename: filename, type: type);
        }

        public Attachment(String data, String filename, String type)
        {
            Init(data: data, filename: filename, type: type);
        }

        public Attachment(long key, long taskKey, byte[] data, String filename, String type)
        {
            Init(key, taskKey, Convert.ToBase64String(data), filename, type);
        }

        public Attachment(long key, long taskKey, List<byte> data, String filename, String type)
        {
            Init(key, taskKey, Convert.ToBase64String(data.ToArray()), filename, type);
        }

        public Attachment(long key, long taskKey, String data, String filename, String type)
        {
            Init(key, taskKey, data, filename, type);
        }

        public Attachment(long taskKey, byte[] data, String filename, String type)
        {
            Init(0, taskKey, Convert.ToBase64String(data), filename, type);
        }

        public Attachment(long taskKey, List<byte> data, String filename, String type)
        {
            Init(0, taskKey, Convert.ToBase64String(data.ToArray()), filename, type);
        }

        public Attachment(long taskKey, String data, String filename, String type)
        {
            Init(0, taskKey, data, filename, type);
        }
        #endregion

        #region Properties
        public long Key { get => this.key; set => this.key = value; }
        public long TaskKey { get => this.taskKey; private set => this.taskKey = value; }
        public String Data { get => this.data; private set => this.data = value; }
        public String Filename { get => this.filename; private set => this.filename = value; }
        public String Type { get => this.type; private set => this.type = value; }
        #endregion
    }
}
