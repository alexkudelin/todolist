﻿using System;
using System.Collections.Generic;

namespace TodoList
{
    namespace Entities
    {
        public class Task
        {
            private long key = 0;
            private long parentKey = 0;

            private String title = "";
            private String subtitle = "";
            private String body = "";

            private DateTime created = DateTime.Now;
            private DateTime doUntil = DateTime.Now;

            private List<Task> children = new List<Task>();

            private short priority = 0;
            private short state = 0; // 0 - не готово, 1 - готово, 2 - удалено

            public Task()
            {
            }

            public Task(Task task)
            {
                this.Key = task.Key;
                this.ParentKey = task.ParentKey;
                this.Title = task.Title;
                this.Subtitle = task.Subtitle;
                this.Body = task.Body;
                this.Created = task.Created;
                this.DoUntil = task.DoUntil;

                foreach (var child in task.Children)
                {
                    this.Children.Add(new Task(child));
                }

                this.Priority = task.Priority;
                this.State = task.State;
            }

            public void Add(Task child)
            {
                this.ParentKey = this.Key;
                this.children.Add(child);

                this.children.Sort((c1, c2) => { return c1.state.CompareTo(c2.state); });
            }

            #region Properties
            public long Key { get => this.key; set => this.key = value; }
            public long ParentKey { get => this.parentKey; set => this.parentKey = value; }
            public String Title { get => this.title; set => this.title = value; }
            public String Subtitle { get => this.subtitle; set => this.subtitle = value; }
            public String Body { get => this.body; set => this.body = value; }
            public DateTime Created { get => this.created; set => this.created = value; }
            public DateTime DoUntil
            {
                get => this.doUntil;
                set
                {
                    if (value.Date >= this.Created.Date)
                    {
                        this.doUntil = value;

                        foreach (var child in this.children)
                        {
                            if (child.DoUntil.Date < this.doUntil)
                            {
                                child.DoUntil = this.doUntil;
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Неправильно задана дата окончания задачи");
                    }
                }
            }
            public short Priority { get => this.priority; set => this.priority = value; }
            public List<Task> Children
            {
                get => this.children;
                set => this.children = value;
            }
            public short State
            {
                get => this.state;
                set
                {
                    this.state = value;

                    if (this.state != 0)
                    {
                        foreach (var child in children)
                        {
                            child.State = this.state;
                        }
                    }
                }
            }

            public int SubtasksDoneCount { get => children.FindAll(task => task.State == 0).Count; }

            public int SubtasksNotDoneCount { get => children.FindAll(task => task.State == 1).Count; }
            #endregion

            public void SortChildren()
            {
                this.children.Sort((c1, c2) => { return c1.state.CompareTo(c2.state); });
            }
        }
    }
}