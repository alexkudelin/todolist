﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoList
{
    public class Service
    {

        public static byte[] ToBytes(String value)
        {
            return Convert.FromBase64String(value);
        }

        public static String ToBase64(byte[] value)
        {
            return Convert.ToBase64String(value);
        }

        public static String JoinWithEscaping(List<String> words, String escapeSymbol = "\"", String separator = ", ")
        {
            List<String> escapedWords = new List<String>();
            foreach (String word in words)
            {
                escapedWords.Add(Escape(word));
            }

            return String.Join(separator, escapedWords);
        }

        public static String JoinWithoutEscaping(List<String> words, String separator = ", ")
        {
            return String.Join(separator, words);
        }

        public static String Escape(String word, String escapeSymbol = "\"")
        {
            return escapeSymbol + word + escapeSymbol;
        }

        public static String BuildPredicate(String paramName, String value, String operatorSign = " = ")
        {
            return String.Join(operatorSign, new List<String>() { Escape(paramName), value });
        }

        public static String BuildWhereStatement(List<Tuple<String, String>> nameValuePairs, String operatorName)
        {
            List<String> predicates = new List<String>();
            foreach (Tuple<String, String> tuple in nameValuePairs)
            {
                predicates.Add(BuildPredicate(tuple.Item1, tuple.Item2));
            }

            return String.Join(" " + operatorName + " ", predicates);
        }
    }
}
